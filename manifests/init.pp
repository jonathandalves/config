class config {

  file { '/etc/puppetlabs/puppet/puppet.conf':
    ensure => present,
    source => 'puppet:///modules/config/agent',
  } 

  exec { 'puppet':
    command => 'systemctl restart puppet',
    path => '/bin',
    subscribe => [
      File['/etc/puppetlabs/puppet/puppet.conf'],
    ],
    refreshonly => true,
  }

}
